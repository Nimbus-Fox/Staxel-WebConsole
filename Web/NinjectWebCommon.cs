﻿using System.Collections.Generic;
using Ninject.Web.Common.WebHost;
using System;
using System.Web;

using Microsoft.Web.Infrastructure.DynamicModuleHelper;

using Ninject;
using Microsoft.AspNet.SignalR;
using Ninject.Web.Common;

namespace NimbusFox.WebConsole.Web {

    public static class NinjectWebCommon {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        public static IKernel Kernel;

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop() {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel() {
            var kernel = new StandardKernel();
            try {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                Kernel = kernel;
                return kernel;
            } catch {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel) {
        }

        public class NinjectSignalRDependencyResolver : IDependencyResolver {
            public object GetService(Type serviceType) {
                return Kernel.TryGet(serviceType);
            }

            public IEnumerable<object> GetServices(Type serviceType) {
                return Kernel.GetAll(serviceType);
            }

            public void Register(Type serviceType, Func<object> activator) {
                Kernel.Bind(serviceType).ToMethod(context => activator());
            }

            public void Register(Type serviceType, IEnumerable<Func<object>> activators) {
                foreach (var activator in activators) {
                    Register(serviceType, activator);
                }
            }

            /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
            public void Dispose() { }
        }
    }
}
