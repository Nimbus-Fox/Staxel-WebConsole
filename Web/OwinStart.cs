﻿using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(NimbusFox.WebConsole.Web.OwinStart))]

namespace NimbusFox.WebConsole.Web {
    internal class OwinStart {
        private string _wwwroot;

        public OwinStart() {
            var dir = new DirectoryInfo(Assembly.GetExecutingAssembly().Location);

            _wwwroot = Path.Combine(dir.Parent.Parent.FullName, "content", "mods", "WebConsole", "wwwroot");
        } 

        public void Configuration(IAppBuilder app) {
            GlobalHost.DependencyResolver = new NinjectWebCommon.NinjectSignalRDependencyResolver();

            app.Run(context => {
                context.Response.ContentType = "text/html";

                return context.Request.Method.ToUpper() == "POST" ? Post(context) : Get(context);
            });

            var hubConfiguration = new HubConfiguration {EnableDetailedErrors = true};
            app.MapSignalR(hubConfiguration);
        }

        private Task Post(IOwinContext context) {


            return context.Response.WriteAsync("404");
        }

        private Task Get(IOwinContext context) {
            if (context.Request.Path.ToString().Contains("./") || context.Request.Path.ToString().Contains("..")) {
                context.Response.StatusCode = 502;
                return context.Response.WriteAsync("502");
            }

            var targetPath = Path.Combine(_wwwroot, context.Request.Path.ToString().Substring(1));

            if (File.Exists(targetPath)) {
                context.Response.ContentType = MimeMapping.GetMimeMapping(targetPath);

                var data = File.ReadAllBytes(targetPath);

                context.Response.ContentLength = data.Length;

                return context.Response.WriteAsync(data);
            }

            if (Directory.Exists(targetPath)) {
                if (File.Exists(Path.Combine(targetPath, "index.html"))) {
                    var data = File.ReadAllText(Path.Combine(targetPath, "index.html"));

                    context.Response.ContentLength = data.Length;

                    return context.Response.WriteAsync(data);
                }
            }

            return GetNonFiles(context);
        }

        private Task GetNonFiles(IOwinContext context) {
            var path = context.Request.Path.ToString();

            if (path == "/") {
                
            }

            context.Response.StatusCode = 404;
            return context.Response.WriteAsync("404");
        }
    }
}
