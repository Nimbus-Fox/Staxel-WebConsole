﻿using System.Diagnostics;
using NimbusFox.WebConsole.Web;
using Plukit.Base;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.WebConsole {
    public class WebHook : IModHookV2 {
        private WebHost _webHost;

        public void Dispose() {
            _webHost.Dispose();
        }
        public void GameContextInitializeInit() { }
        public void GameContextInitializeBefore() { }

        public void GameContextInitializeAfter() {
            if (Process.GetCurrentProcess().ProcessName.StartsWith("Staxel.Server")) {
                _webHost = new WebHost();
            }
        }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return false;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return false;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return false;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }

        public void CleanupOldSession() {
        }
    }
}
