﻿using System;
using Plukit.Base;

namespace NimbusFox.WebConsole.Web {
    internal class WebHost : IDisposable {
        private IDisposable _webserver;

        public WebHost() {
            NinjectWebCommon.Start();

            _webserver = Microsoft.Owin.Hosting.WebApp.Start<OwinStart>("http://127.0.0.1:8080");

            Logger.WriteLine("<WebConsole> Starting webserver on http://127.0.0.1:8080");
        }

        public void Dispose() {
            _webserver.Dispose();
            NinjectWebCommon.Stop();
        }
    }
}
